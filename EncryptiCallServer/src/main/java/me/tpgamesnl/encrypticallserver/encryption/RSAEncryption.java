package me.tpgamesnl.encrypticallserver.encryption;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

/**
 * The encryption class for asymmetric encryption using RSA (specifically RSA/ECB/PKCS1Padding:
 * RSA in Electronic Codebook Mode with PKCS #1 v2.2 padding).
 * This class does not support decryption, because it can only be instantiated with a public key.
 */
public class RSAEncryption implements Encryption {

    private static final String ALGORITHM = "RSA";
    private static final String TRANSFORMATION = "RSA/ECB/PKCS1Padding";

    private Cipher encryptionCipher;

    private RSAEncryption() { }

    public static RSAEncryption fromPublic(byte[] publicKeyBytes) {
        RSAEncryption rsaEncryption = new RSAEncryption();

        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKeyBytes);
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
            PublicKey publicKey = keyFactory.generatePublic(x509EncodedKeySpec);
            rsaEncryption.encryptionCipher = Cipher.getInstance(TRANSFORMATION);
            rsaEncryption.encryptionCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }

        return rsaEncryption;
    }

    @Override
    public byte[] encrypt(byte[] bytes) throws IOException {
        try {
            return encryptionCipher.doFinal(bytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new IOException(e);
        }
    }

    /**
     * Always throws an {@link IllegalStateException} as this class does not support decryption.
     */
    @Override
    public byte[] decrypt(byte[] bytes) throws IllegalStateException {
        throw new IllegalStateException("This class does not support decryption");
    }

}
