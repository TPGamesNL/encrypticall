package me.tpgamesnl.encrypticallserver.encryption;

import java.io.IOException;

/**
 * Any class implementing this interface is used to encrypt / decrypt data, but not necessarily both.
 */
public interface Encryption {

    /**
     * @param bytes The data to encrypt.
     * @return A byte array with the encrypted data.
     * @throws IllegalStateException when the implementing class / instance doesn't support encryption.
     */
    byte[] encrypt(byte[] bytes) throws IOException, IllegalStateException;

    /**
     * @param bytes The data to decrypt.
     * @return A byte array with the decrypted data.
     * @throws IllegalStateException when the implementing class / instance doesn't support decryption.
     */
    byte[] decrypt(byte[] bytes) throws IOException, IllegalStateException;

}
