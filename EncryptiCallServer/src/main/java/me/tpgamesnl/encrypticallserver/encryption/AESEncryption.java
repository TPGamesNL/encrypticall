package me.tpgamesnl.encrypticallserver.encryption;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * The encryption class for asymmetric encryption using AES (specifically AES/CBC/PKCS5Padding).
 * This class supports both encryption and decryption.
 */
public class AESEncryption implements Encryption {

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";

    private Cipher encryptionCipher;
    private Cipher decryptionCipher;

    private AESEncryption() { }

    public static AESEncryption fromSharedSecret(byte[] sharedSecret) {
        AESEncryption aesEncryption = new AESEncryption();

        SecretKeySpec secretKeySpec = new SecretKeySpec(sharedSecret, ALGORITHM);
        try {
            aesEncryption.encryptionCipher = Cipher.getInstance(TRANSFORMATION);
            aesEncryption.encryptionCipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, new IvParameterSpec(sharedSecret));

            aesEncryption.decryptionCipher = Cipher.getInstance(TRANSFORMATION);
            aesEncryption.decryptionCipher.init(Cipher.DECRYPT_MODE, secretKeySpec, new IvParameterSpec(sharedSecret));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            throw new RuntimeException(e);
        }

        return aesEncryption;
    }

    @Override
    public byte[] encrypt(byte[] bytes) throws IOException {
        try {
            return encryptionCipher.doFinal(bytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new IOException(e);
        }
    }

    @Override
    public byte[] decrypt(byte[] bytes) throws IOException {
        try {
            return decryptionCipher.doFinal(bytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new IOException(e);
        }
    }

}
