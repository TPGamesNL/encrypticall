package me.tpgamesnl.encrypticallserver.network.packets;

import me.tpgamesnl.encrypticallserver.network.MyDataInputStream;
import me.tpgamesnl.encrypticallserver.network.MyDataOutputStream;
import me.tpgamesnl.encrypticallserver.network.Packet;

public class DisconnectPacket implements Packet {

    public DisconnectPacket() { }

    @Override
    public void serialize(MyDataOutputStream dos) { }

    @Override
    public void deserialize(MyDataInputStream dis) { }

}
