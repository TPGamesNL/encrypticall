package me.tpgamesnl.encrypticallserver.network.packets;

import me.tpgamesnl.encrypticallserver.network.MyDataInputStream;
import me.tpgamesnl.encrypticallserver.network.MyDataOutputStream;
import me.tpgamesnl.encrypticallserver.network.Packet;

import java.io.IOException;

public class MessagePacket implements Packet {

    private String message;

    private MessagePacket() { }

    @Override
    public void serialize(MyDataOutputStream dos) throws IOException {
        dos.writeUTF(message);
    }

    @Override
    public void deserialize(MyDataInputStream dis) throws IOException {
        message = dis.readUTF();
    }

    public boolean validate() {
        return message.length() <= 128 && message.matches("(\\S| )+");
    }

    public String getMessage() {
        return message;
    }

}
