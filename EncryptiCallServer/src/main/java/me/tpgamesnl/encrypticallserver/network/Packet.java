package me.tpgamesnl.encrypticallserver.network;

import me.tpgamesnl.encrypticallserver.network.MyDataInputStream;
import me.tpgamesnl.encrypticallserver.network.MyDataOutputStream;

import java.io.IOException;

public interface Packet {

    void serialize(MyDataOutputStream dos) throws IOException;

    void deserialize(MyDataInputStream dis) throws IOException;

}
