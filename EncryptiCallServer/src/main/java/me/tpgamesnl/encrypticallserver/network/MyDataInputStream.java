package me.tpgamesnl.encrypticallserver.network;

import org.jetbrains.annotations.NotNull;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyDataInputStream extends DataInputStream {

    /**
     * Creates a DataInputStream that uses the specified
     * underlying InputStream.
     *
     * @param in the specified input stream
     */
    public MyDataInputStream(@NotNull InputStream in) {
        super(in);
    }

    /**
     * Reads a byte array prefixed with its length as an int using {@link #readInt()}.
     */
    public byte[] readByteArray() throws IOException {
        int length = readInt();
        byte[] bytes = new byte[length];
        if (read(bytes) != length)
            throw new IOException("There were not enough bytes available.");
        return bytes;
    }

}
