package me.tpgamesnl.encrypticallserver.network;

import java.io.Closeable;
import java.io.IOException;

public class ClientConnectionThread extends Thread implements Closeable {

    private static final ThreadGroup threadGroup = new ThreadGroup("ClientThreads");

    private final Client client;
    private boolean isActive;

    public ClientConnectionThread(Client client) {
        super(threadGroup, client.toString());
        this.client = client;
    }

    @Override
    public void run() {
        isActive = true;
        while (isActive) {
            try {
                Packet packet = client.readPacket();
                client.handlePacket(packet);
            } catch (IOException e) {
                if (isActive)
                    e.printStackTrace();
                try {
                    if (client.isConnected())
                        client.disconnect();
                } catch (IOException ignored) { }
                break;
            }
        }
    }

    @Override
    public void close() {
        isActive = false;
    }

}
