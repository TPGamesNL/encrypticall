package me.tpgamesnl.encrypticallserver.network;

import me.tpgamesnl.encrypticallserver.network.packets.*;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings("unchecked")
public class PacketRegistration {

    private final static PacketRegistration defaultRegistration = new PacketRegistration();

    static {
        defaultRegistration.registerPacket((byte) 0, PublicKeyPacket.class);
        defaultRegistration.registerPacket((byte) 1, SharedSecretPacket.class);
        defaultRegistration.registerPacket((byte) 2, ServerMessagePacket.class);
        defaultRegistration.registerPacket((byte) 3, MessagePacket.class);
        defaultRegistration.registerPacket((byte) 4, UsernamePacket.class);
        defaultRegistration.registerPacket((byte) 5, DisconnectPacket.class);
        defaultRegistration.registerPacket((byte) 6, KickPacket.class);
    }

    private final Class<?>[] packetClasses = new Class[256];

    public PacketRegistration() { }

    /**
     * Returns the previous packet class associated with the given packet ID.
     */
    public void registerPacket(byte packetId, Class<? extends Packet> packetClass) {
        packetClasses[packetId] = packetClass;
    }

    @Nullable
    public Class<? extends Packet> getPacketClass(byte packetId) {
        return (Class<? extends Packet>) packetClasses[packetId];
    }

    /**
     * @throws UnregisteredPacketException if the given packet class isn't registered.
     */
    public byte getPacketId(Class<? extends Packet> packetClass) throws UnregisteredPacketException {
        if (packetClass == null)
            throw new NullPointerException();

        for (int i = 0; i < 256; i++) {
            if (packetClass.equals(packetClasses[i]))
                return (byte) i;
        }
        throw new UnregisteredPacketException();
    }

    public static PacketRegistration getDefault() {
        return defaultRegistration;
    }

}
