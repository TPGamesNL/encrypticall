package me.tpgamesnl.encrypticallserver.network;

/**
 * This exception is thrown when the application is dealing with unregistered packets.
 */
@SuppressWarnings("unused")
public class UnregisteredPacketException extends PacketException {

    public UnregisteredPacketException() { }

    public UnregisteredPacketException(String message) {
        super(message);
    }

    public UnregisteredPacketException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnregisteredPacketException(Throwable cause) {
        super(cause);
    }

}
