package me.tpgamesnl.encrypticallserver.network.packets;

import me.tpgamesnl.encrypticallserver.network.MyDataInputStream;
import me.tpgamesnl.encrypticallserver.network.MyDataOutputStream;
import me.tpgamesnl.encrypticallserver.network.Packet;

import java.io.IOException;

public class PublicKeyPacket implements Packet {

    private byte[] publicKey;

    private PublicKeyPacket() { }

    public PublicKeyPacket(byte[] publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public void serialize(MyDataOutputStream dos) throws IOException {
        dos.writeByteArray(publicKey);
    }

    @Override
    public void deserialize(MyDataInputStream dis) throws IOException {
        publicKey = dis.readByteArray();
    }

    public byte[] getPublicKey() {
        return publicKey;
    }

}
