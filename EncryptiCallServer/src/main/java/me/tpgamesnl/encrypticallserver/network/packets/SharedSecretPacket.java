package me.tpgamesnl.encrypticallserver.network.packets;

import me.tpgamesnl.encrypticallserver.network.MyDataInputStream;
import me.tpgamesnl.encrypticallserver.network.MyDataOutputStream;
import me.tpgamesnl.encrypticallserver.network.Packet;

import java.io.IOException;

public class SharedSecretPacket implements Packet {

    private byte[] sharedSecret;

    private SharedSecretPacket() { }

    public SharedSecretPacket(byte[] sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    @Override
    public void serialize(MyDataOutputStream dos) throws IOException {
        dos.writeByteArray(sharedSecret);
    }

    @Override
    public void deserialize(MyDataInputStream dis) throws IOException {
        sharedSecret = dis.readByteArray();
    }

    public byte[] getSharedSecret() {
        return sharedSecret;
    }

}
