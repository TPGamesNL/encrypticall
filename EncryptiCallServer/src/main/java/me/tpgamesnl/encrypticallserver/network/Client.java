package me.tpgamesnl.encrypticallserver.network;

import me.tpgamesnl.encrypticallserver.Server;
import me.tpgamesnl.encrypticallserver.encryption.AESEncryption;
import me.tpgamesnl.encrypticallserver.encryption.Encryption;
import me.tpgamesnl.encrypticallserver.encryption.RSAEncryption;
import me.tpgamesnl.encrypticallserver.network.packets.*;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.security.SecureRandom;

public class Client {

    private final Server server;

    private final Socket socket;
    private final InputStream socketInputStream;
    private final OutputStream socketOutputStream;
    private final PacketRegistration packetRegistration;
    private ClientConnectionThread clientConnectionThread;
    private Encryption encryption;

    private String username;

    public Client(Socket socket, PacketRegistration packetRegistration, Server server) throws IOException {
        this.socket = socket;
        this.socketInputStream = socket.getInputStream();
        this.socketOutputStream = socket.getOutputStream();
        this.packetRegistration = packetRegistration;
        this.server = server;

        this.username = "User" + Integer.toHexString(this.hashCode());
    }

    public void start() {
        clientConnectionThread = new ClientConnectionThread(this);
        clientConnectionThread.start();
    }

    public void disconnect() throws IOException {
        socket.close();
        clientConnectionThread.close();
        server.removeClient(this);
    }

    public void kick(String message) throws IOException {
        sendPacket(new KickPacket(message));
        disconnect();
    }

    public boolean isConnected() {
        return socket.isConnected();
    }

    public void sendPacket(Packet packet) throws IOException {
        Class<? extends Packet> packetClass = packet.getClass();
        byte packetId = packetRegistration.getPacketId(packetClass);

        ByteArrayOutputStream packetBodyBaos = new ByteArrayOutputStream();
        MyDataOutputStream packetBodyDos = new MyDataOutputStream(packetBodyBaos);
        packetBodyDos.writeByte(packetId); // Packet ID
        packet.serialize(packetBodyDos); // Packet body

        if (encryption != null) {
            byte[] encryptedBody = encryption.encrypt(packetBodyBaos.toByteArray());

            ByteArrayOutputStream encryptedBaos = new ByteArrayOutputStream(encryptedBody.length + 4);
            MyDataOutputStream encryptedDos = new MyDataOutputStream(encryptedBaos);
            encryptedDos.writeInt(encryptedBody.length); // Packet length, #1
            encryptedDos.write(encryptedBody); // (E) Packet ID + body, #2 + #3

            socketOutputStream.write(encryptedBaos.toByteArray());
        } else {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);

            dos.writeInt(packetBodyBaos.size()); // Packet length, #1
            dos.write(packetBodyBaos.toByteArray()); // Packet ID + body, #2 + #3

            socketOutputStream.write(baos.toByteArray());
        }
    }

    public Packet readPacket() throws IOException, UnknownPacketIdException {
        MyDataInputStream dis = new MyDataInputStream(socketInputStream);

        // Packet length, #1
        int bodyLength = dis.readInt();

        // Packet ID + body, #2 + #3
        byte[] bodyBytes = new byte[bodyLength];
        if (dis.read(bodyBytes) != bodyLength)
            throw new IOException("Length mismatch");

        // Decrypt if needed
        if (encryption != null) {
            bodyBytes = encryption.decrypt(bodyBytes);
        }

        MyDataInputStream bodyDis = new MyDataInputStream(new ByteArrayInputStream(bodyBytes));

        // Packet ID, #2
        byte packetId = bodyDis.readByte();

        // Get the packet class
        Class<? extends Packet> packetClass = packetRegistration.getPacketClass(packetId);
        if (packetClass == null)
            throw new UnknownPacketIdException("Packet ID 0x" + Integer.toHexString(packetId) + " isn't recognized");

        // Instantiate the packet class
        Packet packet;
        try {
            Constructor<? extends Packet> packetConstructor = packetClass.getDeclaredConstructor();
            packetConstructor.setAccessible(true);
            packet = packetConstructor.newInstance();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("The class " + packetClass + " doesn't have a nullary constructor", e);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        // Packet body, #3
        packet.deserialize(bodyDis);
        return packet;
    }

    public void handlePacket(Packet packet) throws IOException {
        if (packet instanceof PublicKeyPacket) {
            System.out.println("Received public key");
            PublicKeyPacket publicKeyPacket = (PublicKeyPacket) packet;
            encryption = RSAEncryption.fromPublic(publicKeyPacket.getPublicKey());

            byte[] sharedSecret = new byte[16];
            new SecureRandom().nextBytes(sharedSecret);

            sendPacket(new SharedSecretPacket(sharedSecret));

            encryption = AESEncryption.fromSharedSecret(sharedSecret);
            System.out.println("Connection encrypted");
        } else if (packet instanceof MessagePacket) {
            MessagePacket messagePacket = (MessagePacket) packet;
            if (!messagePacket.validate()) {
                sendMessage("That message is invalid, it cannot contain new lines.");
                return;
            }
            server.sendMessage(username + "> " + messagePacket.getMessage());
        } else if (packet instanceof UsernamePacket) {
            UsernamePacket usernamePacket = (UsernamePacket) packet;
            if (!usernamePacket.validate()) {
                sendMessage("That username is invalid. It can only contain letters, numbers, underscores and hyphens.");
                return;
            }
            username = usernamePacket.getUsername();
            sendMessage("Your username was changed.");
        } else if (packet instanceof DisconnectPacket) {
            disconnect();
        }
    }

    public void sendMessage(String message) throws IOException {
        sendPacket(new ServerMessagePacket(message));
    }

    public String getUsername() {
        return username;
    }

}
