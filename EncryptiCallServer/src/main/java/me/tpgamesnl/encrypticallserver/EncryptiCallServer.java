package me.tpgamesnl.encrypticallserver;

import java.io.IOException;
import java.util.Scanner;

public class EncryptiCallServer {

    public static void main(String[] args) {
        try {
            Server server = new Server(42069);
            server.start();

            Scanner scanner = new Scanner(System.in);
            while (true) {
                try {
                    String line = scanner.nextLine();
                    if (line.startsWith("/")) {
                        // Command
                        line = line.substring(1);

                        if (line.startsWith("kick")) {
                            line = line.substring("kick ".length());

                            int i = line.indexOf(' ');
                            String username = line.substring(0, i);
                            String reason = line.substring(i + 1);
                            server.kickClient(username, reason);
                            System.out.println("Kicked " + username + " for " + reason);
                        } else {
                            System.out.println("Unknown command");
                        }
                    } else {
                        // Broadcast
                        server.sendMessage("[Broadcast] " + line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
