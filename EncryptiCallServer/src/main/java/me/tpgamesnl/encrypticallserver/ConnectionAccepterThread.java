package me.tpgamesnl.encrypticallserver;

import java.io.Closeable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;

/**
 * This main thread for accepting client connections.
 */
public class ConnectionAccepterThread extends Thread implements Closeable {

    private final ServerSocket serverSocket;
    private final Consumer<Socket> socketConsumer;
    private final Consumer<Exception> exceptionHandler;
    private boolean isActive = false;

    /**
     * @param serverSocket The ServerSocket this thread deals with.
     * @param socketConsumer The Consumer that gets called for every accepted Socket.
     * @param exceptionHandler The Consuemr that gets called for every exception.
     */
    public ConnectionAccepterThread(ServerSocket serverSocket,
                                    Consumer<Socket> socketConsumer,
                                    Consumer<Exception> exceptionHandler) {
        super("ConnectionAccepterThread");
        this.setDaemon(true);
        this.serverSocket = serverSocket;
        this.socketConsumer = socketConsumer;
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    public void run() {
        isActive = true;
        while (isActive) {
            try {
                socketConsumer.accept(serverSocket.accept());
            } catch (Exception e) {
                exceptionHandler.accept(e);
            }
        }
    }

    /**
     * Ends this thread indirectly. Use {@link Thread#isAlive()} to see whether the thread has finished.
     */
    @Override
    public void close() {
        this.isActive = false;
    }

}
