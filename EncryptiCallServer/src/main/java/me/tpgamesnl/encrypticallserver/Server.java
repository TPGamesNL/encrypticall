package me.tpgamesnl.encrypticallserver;

import me.tpgamesnl.encrypticallserver.network.Client;
import me.tpgamesnl.encrypticallserver.network.PacketRegistration;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Server implements Closeable {

    private final int port;
    private ConnectionAccepterThread connectionAccepterThread;

    private final ArrayList<Client> clients = new ArrayList<>();

    public Server(int port) {
        this.port = port;
    }

    public void start() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        connectionAccepterThread = new ConnectionAccepterThread(
                serverSocket,
                this::handleConnection,
                this::handleException);
        connectionAccepterThread.start();
        System.out.println("Server started");
    }

    public void handleConnection(Socket socket) {
        try {
            Client client = new Client(socket, PacketRegistration.getDefault(), this);
            client.start();
            clients.add(client);
        } catch (IOException e) {
            handleException(e);
        }
    }

    public void handleException(Exception exception) {
        exception.printStackTrace();
    }

    @Override
    public void close() {
        connectionAccepterThread.close();
    }

    /**
     * Removes a client from the list, but does not disconnect them.
     */
    public void removeClient(Client client) {
        clients.remove(client);
    }

    /**
     * Returns all connected clients.
     */
    public List<Client> getClients() {
        // Just to be sure
        return clients.stream()
                .filter(Client::isConnected)
                .collect(Collectors.toList());
    }

    public void kickClient(String username, String message) throws IOException {
        for (Client client : getClients()) {
            if (client.getUsername().equals(username))
                client.kick(message);
        }
    }

    public void sendMessage(String message) throws IOException {
        for (Client client : clients)
            client.sendMessage(message);
        System.out.println("[Chat] " + message);
    }

}
