# EncryptiCall

This repository contains 2 projects: EncryptiCallClient and EncryptiCallServer.
Both are maven projects that can be build by running `mvn package` in their respective directory.

Client commands:
- /username <string>: set your username
- /disconnect: disconnect, alias /dc

Server commands:
- /kick <string> <string>: kick the client with name arg-1, with the kick message arg-2

Server operates on port 42069, the client automatically connects to localhost (hardcoded, sorry).
