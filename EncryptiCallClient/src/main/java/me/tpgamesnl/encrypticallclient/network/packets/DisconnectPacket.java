package me.tpgamesnl.encrypticallclient.network.packets;

import me.tpgamesnl.encrypticallclient.network.MyDataInputStream;
import me.tpgamesnl.encrypticallclient.network.MyDataOutputStream;
import me.tpgamesnl.encrypticallclient.network.Packet;

public class DisconnectPacket implements Packet {

    public DisconnectPacket() { }

    @Override
    public void serialize(MyDataOutputStream dos) { }

    @Override
    public void deserialize(MyDataInputStream dis) { }

}
