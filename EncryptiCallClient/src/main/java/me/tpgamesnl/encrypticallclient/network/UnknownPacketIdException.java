package me.tpgamesnl.encrypticallclient.network;

/**
 * This exception is thrown whenever a packet is read with an invalid packet ID.
 */
public class UnknownPacketIdException extends PacketException {

    public UnknownPacketIdException() {
    }

    public UnknownPacketIdException(String message) {
        super(message);
    }

    public UnknownPacketIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownPacketIdException(Throwable cause) {
        super(cause);
    }
}
