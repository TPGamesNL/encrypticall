package me.tpgamesnl.encrypticallclient.network.packets;

import me.tpgamesnl.encrypticallclient.network.MyDataInputStream;
import me.tpgamesnl.encrypticallclient.network.MyDataOutputStream;
import me.tpgamesnl.encrypticallclient.network.Packet;

import java.io.IOException;

public class SharedSecretPacket implements Packet {

    private byte[] sharedSecret;

    private SharedSecretPacket() { }

    public SharedSecretPacket(byte[] sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    @Override
    public void serialize(MyDataOutputStream dos) throws IOException {
        dos.writeByteArray(sharedSecret);
    }

    @Override
    public void deserialize(MyDataInputStream dis) throws IOException {
        sharedSecret = dis.readByteArray();
    }

    public byte[] getSharedSecret() {
        return sharedSecret;
    }

}
