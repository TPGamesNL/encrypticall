package me.tpgamesnl.encrypticallclient.network.packets;

import me.tpgamesnl.encrypticallclient.network.MyDataInputStream;
import me.tpgamesnl.encrypticallclient.network.MyDataOutputStream;
import me.tpgamesnl.encrypticallclient.network.Packet;

import java.io.IOException;

public class ServerMessagePacket implements Packet {

    private String message;

    private ServerMessagePacket() { }

    public ServerMessagePacket(String message) {
        this.message = message;
    }

    @Override
    public void serialize(MyDataOutputStream dos) throws IOException {
        dos.writeUTF(message);
    }

    @Override
    public void deserialize(MyDataInputStream dis) throws IOException {
        message = dis.readUTF();
    }

    public String getMessage() {
        return message;
    }

}
