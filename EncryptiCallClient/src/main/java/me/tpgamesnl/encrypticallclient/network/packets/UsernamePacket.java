package me.tpgamesnl.encrypticallclient.network.packets;

import me.tpgamesnl.encrypticallclient.network.MyDataInputStream;
import me.tpgamesnl.encrypticallclient.network.MyDataOutputStream;
import me.tpgamesnl.encrypticallclient.network.Packet;

import java.io.IOException;

public class UsernamePacket implements Packet {

    private String username;

    private UsernamePacket() { }

    public UsernamePacket(String username) {
        this.username = username;
    }

    @Override
    public void serialize(MyDataOutputStream dos) throws IOException {
        dos.writeUTF(username);
    }

    @Override
    public void deserialize(MyDataInputStream dis) throws IOException {
        username = dis.readUTF();
    }

    public boolean validate() {
        return username.length() <= 16 && username.matches("[a-zA-Z0-9-_]+");
    }

    public String getUsername() {
        return username;
    }

}
