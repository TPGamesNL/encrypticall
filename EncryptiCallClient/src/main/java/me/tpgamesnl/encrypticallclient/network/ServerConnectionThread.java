package me.tpgamesnl.encrypticallclient.network;

import java.io.Closeable;
import java.io.IOException;

public class ServerConnectionThread extends Thread implements Closeable {

    private final Client client;
    private boolean isActive;

    public ServerConnectionThread(Client client) {
        this.client = client;
    }

    @Override
    public void run() {
        isActive = true;
        while (isActive) {
            try {
                Packet packet = client.readPacket();
                client.handlePacket(packet);
            } catch (IOException e) {
                if (isActive)
                    e.printStackTrace();
                break;
            }
        }
    }

    @Override
    public void close() {
        isActive = false;
    }

}
