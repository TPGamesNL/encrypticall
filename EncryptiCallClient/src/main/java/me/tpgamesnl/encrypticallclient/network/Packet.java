package me.tpgamesnl.encrypticallclient.network;

import me.tpgamesnl.encrypticallclient.network.MyDataInputStream;
import me.tpgamesnl.encrypticallclient.network.MyDataOutputStream;

import java.io.IOException;

public interface Packet {

    void serialize(MyDataOutputStream dos) throws IOException;

    void deserialize(MyDataInputStream dis) throws IOException;

}
