package me.tpgamesnl.encrypticallclient.network;

import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MyDataOutputStream extends DataOutputStream {

    /**
     * Creates a new data output stream to write data to the specified
     * underlying output stream. The counter <code>written</code> is
     * set to zero.
     *
     * @param out the underlying output stream, to be saved for later
     *            use.
     * @see FilterOutputStream#out
     */
    public MyDataOutputStream(OutputStream out) {
        super(out);
    }

    /**
     * Writes a byte array, prefixed with its length as an int using {@link #writeInt(int)}.
     */
    public void writeByteArray(byte[] bytes) throws IOException {
        writeInt(bytes.length);
        write(bytes);
    }

}
