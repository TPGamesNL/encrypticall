package me.tpgamesnl.encrypticallclient.network;

import me.tpgamesnl.encrypticallclient.encryption.AESEncryption;
import me.tpgamesnl.encrypticallclient.encryption.Encryption;
import me.tpgamesnl.encrypticallclient.encryption.RSAEncryption;
import me.tpgamesnl.encrypticallclient.network.packets.*;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.Socket;

public class Client {

    private ServerConnectionThread serverConnectionThread;
    private Socket socket;
    private InputStream socketInputStream;
    private OutputStream socketOutputStream;

    private PacketRegistration packetRegistration;
    private Encryption encryption;

    public Client(InetAddress inetAddress, int port) throws IOException {
        connect(inetAddress, port);
    }

    public void connect(InetAddress inetAddress, int port) throws IOException {
        this.socket = new Socket(inetAddress, port);
        this.socketInputStream = socket.getInputStream();
        this.socketOutputStream = socket.getOutputStream();
        this.packetRegistration = PacketRegistration.getDefault();
        this.encryption = null;
        System.out.println("Connected");

        serverConnectionThread = new ServerConnectionThread(this);
        serverConnectionThread.start();

        RSAEncryption rsaEncryption = RSAEncryption.fromRandom();
        try {
            System.out.println("Initiating encryption");
            sendPacket(new PublicKeyPacket(rsaEncryption.getPublicKey()));
            encryption = rsaEncryption;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void disconnect(boolean sendDisconnect) throws IOException {
        if (sendDisconnect) {
            try {
                sendPacket(new DisconnectPacket());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        serverConnectionThread.close();
        socket.close();
    }

    public void sendPacket(Packet packet) throws IOException {
        Class<? extends Packet> packetClass = packet.getClass();
        byte packetId = packetRegistration.getPacketId(packetClass);

        ByteArrayOutputStream packetBodyBaos = new ByteArrayOutputStream();
        MyDataOutputStream packetBodyDos = new MyDataOutputStream(packetBodyBaos);
        packetBodyDos.writeByte(packetId); // Packet ID
        packet.serialize(packetBodyDos); // Packet body

        if (encryption != null) {
            byte[] encryptedBody = encryption.encrypt(packetBodyBaos.toByteArray());

            ByteArrayOutputStream encryptedBaos = new ByteArrayOutputStream(encryptedBody.length + 4);
            MyDataOutputStream encryptedDos = new MyDataOutputStream(encryptedBaos);
            encryptedDos.writeInt(encryptedBody.length); // Packet length, #1
            encryptedDos.write(encryptedBody); // (E) Packet ID + body, #2 + #3

            socketOutputStream.write(encryptedBaos.toByteArray());
        } else {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);

            dos.writeInt(packetBodyBaos.size()); // Packet length, #1
            dos.write(packetBodyBaos.toByteArray()); // Packet ID + body, #2 + #3

            socketOutputStream.write(baos.toByteArray());
        }
    }

    public Packet readPacket() throws IOException, UnknownPacketIdException {
        MyDataInputStream dis = new MyDataInputStream(socketInputStream);

        // Packet length, #1
        int bodyLength = dis.readInt();

        // Packet ID + body, #2 + #3
        byte[] bodyBytes = new byte[bodyLength];
        if (dis.read(bodyBytes) != bodyLength)
            throw new IOException("Length mismatch");

        // Decrypt if needed
        if (encryption != null) {
            bodyBytes = encryption.decrypt(bodyBytes);
        }

        MyDataInputStream bodyDis = new MyDataInputStream(new ByteArrayInputStream(bodyBytes));

        // Packet ID, #2
        byte packetId = bodyDis.readByte();

        // Get the packet class
        Class<? extends Packet> packetClass = packetRegistration.getPacketClass(packetId);
        if (packetClass == null)
            throw new UnknownPacketIdException("Packet ID 0x" + Integer.toHexString(packetId) + " isn't recognized");

        // Instantiate the packet class
        Packet packet;
        try {
            Constructor<? extends Packet> packetConstructor = packetClass.getDeclaredConstructor();
            packetConstructor.setAccessible(true);
            packet = packetConstructor.newInstance();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("The class " + packetClass + " doesn't have a nullary constructor", e);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        // Packet body, #3
        packet.deserialize(bodyDis);
        return packet;
    }

    public void handlePacket(Packet packet) throws IOException {
        if (packet instanceof SharedSecretPacket) {
            byte[] sharedSecret = ((SharedSecretPacket) packet).getSharedSecret();
            encryption = AESEncryption.fromSharedSecret(sharedSecret);
            System.out.println("Connection encrypted");
        } else if (packet instanceof ServerMessagePacket) {
            ServerMessagePacket serverMessagePacket = (ServerMessagePacket) packet;
            System.out.println(serverMessagePacket.getMessage());
        } else if (packet instanceof KickPacket) {
            System.out.println("You've been kicked due to " + ((KickPacket) packet).getMessage());
            disconnect(false);
        }
    }

    public void sendMessage(String message) throws IOException {
        sendPacket(new MessagePacket(message));
    }

}
