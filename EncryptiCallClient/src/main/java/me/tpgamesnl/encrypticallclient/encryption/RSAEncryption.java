package me.tpgamesnl.encrypticallclient.encryption;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

/**
 * The encryption class for asymmetric encryption using RSA.
 * This class supports both encryption and decryption.
 */
public class RSAEncryption implements Encryption {

    private static final String ALGORITHM = "RSA";
    private static final String TRANSFORMATION = "RSA/ECB/PKCS1Padding";

    private KeyPair keyPair;
    private Cipher encryptionCipher;
    private Cipher decryptionCipher;

    private RSAEncryption() { }

    public static RSAEncryption fromRandom() {
        RSAEncryption rsaEncryption = new RSAEncryption();

        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();

            rsaEncryption.encryptionCipher = Cipher.getInstance(TRANSFORMATION);
            rsaEncryption.encryptionCipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

            rsaEncryption.decryptionCipher = Cipher.getInstance(TRANSFORMATION);
            rsaEncryption.decryptionCipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());

            rsaEncryption.keyPair = keyPair;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }

        return rsaEncryption;
    }

    @Override
    public byte[] encrypt(byte[] bytes) throws IOException {
        try {
            return encryptionCipher.doFinal(bytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new IOException(e);
        }
    }

    @Override
    public byte[] decrypt(byte[] bytes) throws IOException {
        try {
            return decryptionCipher.doFinal(bytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new IOException(e);
        }
    }

    public byte[] getPublicKey() {
        return keyPair.getPublic().getEncoded();
    }

}
