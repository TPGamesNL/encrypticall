package me.tpgamesnl.encrypticallclient;

import me.tpgamesnl.encrypticallclient.network.Client;
import me.tpgamesnl.encrypticallclient.network.packets.UsernamePacket;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Scanner;

public class EncryptiCallClient {

    public static void main(String[] args) throws IOException {
        InetAddress inetAddress = InetAddress.getByName("localhost");
        Client client = new Client(inetAddress, 42069);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                String line = scanner.nextLine();

                if (line.startsWith("/")) {
                    // Command
                    if (line.startsWith("/username ")) {
                        String username = line.substring("/username ".length());
                        client.sendPacket(new UsernamePacket(username));
                    } else if (line.equals("/dc") || line.equals("/disconnect")) {
                        client.disconnect(true);
                        System.out.println("Disconnected");
                    } else {
                        System.out.println("Unknown command");
                    }
                } else {
                    // Message
                    client.sendMessage(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
    }

}
