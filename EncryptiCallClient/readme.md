EncryptiCall bestaat uit twee onderdelen: de server en de client. Beiden hebben deels gedeelde code, en deels 
hun eigen code. 

## Netwerk
De communicatie tussen de twee gaat via TCP sockets met behulp van packets, en alle data die wordt verzonden staat in big-endian. De volgende data types gebruiken we:

| Data type | Aantal bytes | Uitleg |
| --------- | ------------ | ------ |
| byte | 1 | Een byte bestaat uit 8 bits, en een byte is het simpelste data type. Een byte is gesigneerd, en heeft een waarde van -128 tot 127. |
| int | 4 | Een int is een gesigneerd geheel getal, met een minimale waarde van -2<sup>31</sup> en een maximale waarde van 2<sup>31</sup>-1. |
| X array | variabel | Een array is een lijst, waarbij X staat voor het type van het element waaruit de lijst bestaat. De lengte van de lijst en het aantal bytes van dit type hangt af van de context. |
| String | variabel | Een String is een tekst. De tekst wordt voorafgegaan door het aantal bytes dat volgt. Deze bytes bestaan uit de door UTF-8 gecodeerde tekst. De variant van UTF-8 is lichtelijk aangepast, zie daarvoor [deze link](https://docs.oracle.com/javase/8/docs/api/java/io/DataInput.html#modified-utf-8) |

Alle packets zijn in het volgende formaat:

| Veldnaam | Data type | Uitleg |
| -------- | --------- | ------ |
| Lengte | int | Het totale aantal bytes van deze packet (exclusief de 4 bytes van dit veld). |
| Packet ID | byte | Een ID dat verschilt per packet-type. |
| Packet data | byte array | Deze byte array bestaat uit de resterende bytes van deze packet, en de betekenis van deze bytes wordt bepaald door het packet-type. |

### Packets
Van de packets die hieronder wordt eerst het packet ID en de netwerkrichting aangegeven, of de packet 
van de client naar de server gaat of andersom, daarna wordt het formaat weergegeven van het Packet data veld, 
en mogelijk daaronder nog extra uitleg voor de packet.
De richting wordt aangegeven als C->S voor een packet die verstuurd wordt vanuit de client, naar de server toe. 
Het omgekeerde, S->C, staat voor een packet richting de client, vanuit de server.

#### PublicKeyPacket
ID: 0 \
Richting: C->S

| Veldnaam | Data type | Uitleg |
| -------- | --------- | ------ |
| Sleutellengte | int | De lengte (in bytes) van de sleutel. |
| Sleutel | byte array | De sleutel. |

Deze packet wordt gebruikt om een publieke sleutel te versturen naar de server. 

#### SharedSecretPacket
ID: 1 \
Richting: S->C

| Veldnaam | Data type | Uitleg |
| -------- | --------- | ------ |
| Sleutellengte | int | De lengte (in bytes) van de sleutel. |
| Sleutel | byte array | De sleutel. |

Deze packet wordt gebruikt om een gedeelde sleutel terug te sturen naar de client.

#### ServerMessagePacket
ID: 2 \
Richting: S->C

| Veldnaam | Data type | Uitleg |
| -------- | --------- | ------ |
| Bericht | String | Het bericht dat wordt verzonden. |

Deze packet wordt gebruikt als de server een bericht aan de client wil versturen, zoals een bericht dat van een 
andere verbonden client af komt, of een mededeling van de server.

#### MessagePacket
ID: 3 \
Richting: C->S

| Veldnaam | Data type | Uitleg |
| -------- | --------- | ------ |
| Bericht | String | Het bericht dat wordt verzonden. |

Deze packet is bijna hetzelfde als de [ServerMessagePacket](#servermessagepacket), en het enige verschil is dat deze 
wordt gebruikt voor een bericht dat de client naar de server stuurt (een chatbericht). Het bericht mag niet langer dan 
128 letters zijn, en mag geen nieuwe regel karakters bevatten, wat wordt gecontroleerd met de RegEx `(\S| )+`.

#### UsernamePacket
ID: 4 \
Richting: C->S

| Veldnaam | Data type | Uitleg |
| -------- | --------- | ------ |
| Username | String | De gewenste username. |

Deze packet wordt verstuurd wanneer de client zijn/haar username wil veranderen, deze is zichtbaar wanneer de client
een bericht stuurt, door de [MessagePacket](#messagepacket). De server reageert op deze packet met een 
[ServerMessagePacket](#servermessagepacket), met het bericht "Your username was changed." wanneer de username 
geaccepteerd wordt, en "That username is invalid. It can only contain letters, numbers, underscores and hyphens." 
wanneer de username niet geldig is. De username is ongeldig wanneer hij langer dan 16 karakters is, of wanneer deze 
andere tekens dan letters, cijfers, lage streepjes en koppeltekens gebruikt, dit wordt gecontroleerd met de RegEx
`[a-zA-Z0-9-_]+`. 

#### DisconnectPacket
ID: 5 \
Richting: C->S

Deze packet heeft geen velden, en wordt alleen gebruikt om aan te geven dat de client de verbinding met de server 
wil verbreken.

#### KickPacket
ID: 6 \
Richting: S->C

| Veldnaam | Data type | Uitleg |
| -------- | --------- | ------ |
| Reden | String | De reden voor de kick. |

Deze packet wordt gebruikt door de server om aan te geven dat de verbinding met de client zal worden verbroken.   

## Encryptie

### Formaat versleutelde packet
Wanneer een packet versleuteld wordt verstuurd, worden de velden Packet ID en Packet data versleuteld, en wordt het veld
Lengte gebruikt voor de lengte van het versleutelde gedeelte, niet de lengte van de onversleutelde data.

### Asymmetrisch
De client verbind met de server, en stuurt vervolgens een [PublicKeyPacket](#publickeypacket), waarin een publieke 
sleutel te vinden is die gemaakt is met RSA in ECB-modus, Electronic Code Book modus, met OAEP padding.

#### ECB en CBC
ECB en CBC zijn twee modi voor blokvercijfering, het versleutelen van een blok gegevens. ECB is de simpelste modus, 
deze volgt het algoritme dat wordt gebruikt voor de versleuteling precies. Het probleem met ECB, is dat twee identieke 
blokken data dezelfde cijfertekst teruggeven, wat betekent dat een afluisteraar, die naar de versleutelde gegevens 
kijkt, kan zien dat de blokken data identiek zijn. CBC lost dit op door elke packet te veranderen, door de logische
operatie XOR erop toe te passen met de vorige packet als tweede input. In het geval dat het blok dat moet worden 
versleuteld het eerste is, dan wordt de initialisatievector gebruikt, dit is een set getallen die bij de server en 
de client bekend zijn, vaak wordt hiervoor de sleutel van het gebruikte algoritme gebruikt. 

#### Padding
Padding is het toevoegen van data aan de te versleutelen data, voor het versleutelen. Dit kan worden gebruikt om twee 
identieke blokken data te laten verschillen, of om data die niet voldoet aan de vereiste bloklengte langer te maken, 
zodat deze data wel lang genoeg is. OAEP, Optimal Asymmetric Encryption Padding, is een methode voor padding, die is 
gestandaardiseerd door PKCS, de Public-Key Cryptography Standards.

### Symmetrisch
Nadat de server de public key van de client heeft ontvangen maakt de server een sleutel van 16 bytes lang, die zal 
worden gebruikt voor de encryptie van alle komende packets. Dit zal worden gedaan met AES in CBC-modus, met PKCS#5 
padding. De sleutel wordt verzonden in een [SharedSecretPacket](#sharedsecretpacket), en de packet zelf is versleuteld 
met RSA, zoals uitgelegd onder [Asymmetrisch](#asymmetrisch). 

#### PKCS#5 padding
PKCS#5 padding is een eenvoudige vorm van padding: stel, je komt `n` bytes tekort voor een volledig blok. Je vult het
blok aan met `n` bytes, elk met de waarde `n`. 